/**
 * Shuffles array in place. ES6 version
 * @param {Array} a items An array containing the items.
 */
function shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}

// LECTEUR AUDIO :
const player = document.querySelector('#audioPlayer');

// Click maxi pour la victoire :
const VICTORY_CLICK_MAXI = 10;

// LANCEMENT DU JEU :

// Quand on click sur le bouton GO :
function newgame() {

    const images = ({
        1: "harry-potter.jpg",
        2: "hermione-granger.jpg",
        3: "ron-weasley.jpg",
        4: "neville-londubat.jpg",
        5: "drago-malefoy.jpg",
        6: "albus-dumbledore.jpg",
        7: "minerva-mc-gonagall.jpg",
        8: "ginny-weasley.jpg",
        9: "rubeus-hagrid.jpg",
        10: "severus-rogue.jpg",
        11: "sirius-black.jpg",
        12: "nyphadora-tonks.jpg"
        // ...
    });

    // On réinitialise la table de jeu :
    resetGame();

    // on récupère le nombre de cartes Uniques choisi par l'utilisateur :
    let cardsNumber = Number(document.getElementById('cardNumber').value);

    // Tableau de valeur des cartes :
    let carte = [];

    // On genere un tableau en fonction de la saisie ;
    for (let i = 0; i < cardsNumber; i++) {
        carte.push(i + 1, i + 1);
    }

    // SHUFFLE :
    // On melange le tableau, et on récupere le résultat dans "donne" :
    const donne = [] = shuffle(carte);

    // On affiche le resultat dans la console :
    console.log("jeu2carte : ", donne);

    // On crée une table de jeu :
    for (let i = 0; i < donne.length; i++) {
        document.getElementById('board').innerHTML +=
            '<button class="htmlButtons image" value="' + donne[i] + '" id="carte' + i + '"></button>';
    }

    // On met chaque button html (carte) crée dans une constante,
    const htmlButtons = document.querySelectorAll('.htmlButtons');

    // On initialise 2 tableaux, 
    // - Un qui récupère les valeurs cliquées
    // - Un autre qui récupère l'id des boutons :
    let cardsValue = [];
    let cardsId = [];

    // On determine un compteur de click :
    let click = 0;
    if ((donne.length / 2) <= 3) {
        click = donne.length + 2;
    }
    else if ((donne.length / 2) >= 4) {
        click = donne.length + VICTORY_CLICK_MAXI;
    }
    document.getElementById('clicks').innerHTML = click;

    // On prepare un boolean pour la victoire :
    let isFinished = false;


    // Pour chaque button, 
    for (let i = 0; i < htmlButtons.length; i++) {
        // On écoute le click :
        htmlButtons[i].addEventListener('click', function (e) {

            // On ajoute un click :
            click--;
            document.getElementById('clicks').innerHTML = click;

            // On affiche la valeur de la carte cliquée :
            htmlButtons[i].classList.remove('image');
            htmlButtons[i].innerHTML = '<img class="portraits" src="../photos/' + images[e.target.value] + '">';
            htmlButtons[i].style.color = 'black';
            htmlButtons[i].style.animation = '';


            // On ajoute au tableaux crées la valeur cliquée, puis l'id du boutons :
            cardsValue.push(e.target.value);
            cardsId.push(i);

            console.log(cardsValue);
            console.log(cardsId);

            // Si on a cliqué deux fois
            // (on securise le jeu)
            // On prefere >= a === car toutes les valeurs au desus de 2 sont exclues :
            if (cardsValue.length >= 2) {

                // On bloque le click :
                e.preventDefault();

                // Si les valeurs sont differentes,
                // Ou que l'on a cliqué sur la même carte deux fois :
                if (cardsValue[0] !== cardsValue[1] || cardsId[0] === cardsId[1]) {

                    // PERDU !
                    console.log("RETRY")

                    // On récupere les deux valeurs du tableau Id Buttons,
                    // pour remettre l'affichage a zéro sur les cartes choisies :
                    let a = cardsId[0];
                    let b = cardsId[1];

                    // On retourne les cartes en affichant l'image, et le texte:
                    setTimeout(function () {
                        htmlButtons[a].innerHTML = "";
                        htmlButtons[b].innerHTML = "";
                        htmlButtons[a].classList.add('image');
                        htmlButtons[b].classList.add('image');
                        htmlButtons[a].style.animation = 'cardturn 0.5s';
                        htmlButtons[b].style.animation = 'cardturn 0.5s';
                    }, 500)

                }
                else {

                    // GAGNE !
                    console.log("VICTORY !")

                    // On met le son en variable :
                    let gagne = document.getElementById('gagne');
                    gagne.play();
                    gagne.volume = 0.2;

                    // On prepare unn tableau vide :
                    let values = [];

                    // Pour chaque carte qui affiche une valeur
                    // on l'inclu dans le tableau :
                    htmlButtons.forEach(e => {
                        if (e.innerHTML != '') {
                            values.push(e.innerHTML);
                        }
                    });

                    // Si la longueur des deux tableaux sont identiques :
                    if (values.length === donne.length) {
                        console.log("WON THE GAME !!!!!!");

                        // On lance le son de fin de partie :
                        let victorySound = document.getElementById('victorysound');
                        victorySound.play();
                        victorySound.volume = 0.2;

                        // On ferme le jeu grace au boolean "isFinished" :
                        isFinished = true;

                        // On envoye la function de fin de jeu : 
                        endGame(click, donne.length);

                        return;
                    }
                }

                // On réinitialise les tableaux :
                cardsValue = [];
                cardsId = [];
            }
        })
    }

}

function resetGame() {
    document.getElementById('board').innerHTML = '';
    player.play();
}

function play(idPlayer, control) {
    if (player.paused) {
        player.play();
        control.innerHTML = '<i class="fas fa-pause-circle"></img>';
    } else {
        player.pause();
        control.innerHTML = '<i class="fas fa-play-circle"></i>';
    }
}

function resume(idPlayer) {
    document.getElementById('play').innerHTML = '<i class="fas fa-play-circle"></i>';
    player.currentTime = 0;
    player.pause();
}

let vol = document.getElementById('volume');
vol.addEventListener('input', function () {
    player.volume = vol.value;
    console.log(vol.value);
}, false);



// Get the modal
let modal = document.getElementById("myModal");

// Get the image and insert it inside the modal - use its "alt" text as a caption
let img = document.getElementById("myImg");
let modalImg = document.getElementById("img01");
let captionText = document.getElementById("caption");
let captionTitle = document.getElementById("captionTitle");


// On determine les types de victoires possibles
//  - Master  = jeu de carte + 2
//  - Victory = jeu de carte + 8
//  - Defeat 
function endGame(click, donne) {

    modal.style.display = "block";

    // Si l'utilisateur a choisi entre 1 et 3 paires :
    if (donne / 2 <= 3) {
        console.log("click et donne : ", click, donne)
        // Victory :
        if (click <= donne - 2 && click >= 0) {
            modalImg.src = "images/victory.jpg"
            captionTitle.innerHTML = "Victory !!"
            captionText.innerHTML = "<h4>Celui dont on ne prononce pas le nom a fuit !</h4> <br> <h2>Braaaavo</h2>";
        }
        // Defeat :
        else if (click <= 0) {
            modalImg.src = "images/voldemort.jpg"
            captionTitle.innerHTML = "Perdu .."
            captionText.innerHTML = "<h4>Celui dont on ne prononce pas le nom a vaincu !</h4> <br> <h2>Excerce ta memoire de magicien ...</h2>";
        }
    }

    // Si l'utilisateur a choisi plus de 3 paires :
    if (donne / 2 >= 4) {
        // Master :
        if (click >= donne - 2 && click >= 0) {
            modalImg.src = "images/master.jpg";
            captionTitle.innerHTML = "Grand Magicien !"
            captionText.innerHTML = "<h4>Celui dont on ne prononce pas le nom n'existe plus !!</h4> <br> <h2>C'est toi Dumbledore ??</h2>";
        }
        // Victory :
        else if (click <= donne - 2 && click >= donne - VICTORY_CLICK_MAXI && click >= 0) {
            modalImg.src = "images/victory.jpg"
            captionTitle.innerHTML = "Victory !!"
            captionText.innerHTML = "<h4>Celui dont on ne prononce pas le nom a fuit !</h4> <br> <h2>Braaaavo</h2>";
        }
        // Defeat :
        else if (click <= 0) {
            modalImg.src = "images/voldemort.jpg"
            captionTitle.innerHTML = "Perdu .."
            captionText.innerHTML = "<h4>Celui dont on ne prononce pas le nom a vaincu !</h4> <br> <h2>Excerce ta memoire de magicien ...</h2>";
        }
    }

}

// Get the <span> element that closes the modal
let span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function () {
    modal.style.display = "none";
}